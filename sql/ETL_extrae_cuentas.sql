/* Cuentas
 * Devuelve las cuentas
 * @param 
 * @return cta_id, cta_producto, cta_estado, cta_titular, cta_fechaalta, cta_deudainicial, cta_deudafinal
 * @author  Fernando Aguirre
 * @version 1 Fernando Aguirre 19/7/21
 */
 
with EstadosCta(cta_id, cta_estado) as
(
  select ec.esc_cuenta      
       , pa.par_valorstring
    from EstadoCuenta ec
         inner join Parametro pa 
          ON pa.par_id = ec.esc_estado 
  where ec.esc_fechainicio = (select max(ecmax.esc_fechainicio)
                                 from EstadoCuenta ecmax
                                where ec.esc_cuenta = ecmax.esc_cuenta)
 )

, DeudaInicial(cta_id, dta_fechaalta, cta_deudainicial) as
    (
        select distinct deu_cuenta
             , deu_fechaalta
             , deu_capital + deu_interes + deu_honorarios + deu_gastos
          from Deuda
         where cast(deu_fechaalta as date) = (select min(cast(dmin.deu_fechaalta as date))
                                  from Deuda dmin
                                 where dmin.deu_cuenta = Deuda.deu_cuenta)
               and deu_cuenta is not null
    )

, DeudaActual(cta_id, dta_fechaalta, cta_deudaactual) as
         (
             select distinct deu_cuenta
                  , deu_fechaalta
                  , deu_capital + deu_interes + deu_honorarios + deu_gastos
             from Deuda
             where cast(deu_fechaalta as date) = (select max(cast(dmax.deu_fechaalta as date))
                                    from Deuda dmax
                                    where dmax.deu_cuenta = Deuda.deu_cuenta)
         )

select cta.cta_id           cta_id
     , prd.pro_nombre       cta_producto
     , esc.cta_estado       cta_estado
     , cta.cta_titular      cta_titular
     , cta.cta_fechaalta    cta_fechaalta
     , din.cta_deudainicial cta_deudainicial
     , dac.cta_deudaactual  cta_deudaactual

  from Cuenta cta
  
       inner join EstadosCta esc
        on cta.cta_id = esc.cta_id
       
       inner join Producto prd
        on cta.cta_producto = prd.pro_id

        left join DeudaInicial din
            on cta.cta_id = din.cta_id

        left join DeudaActual dac
            on cta.cta_id = dac.cta_id
        