/* Convenios
 * Devuelve los convenios de plan de pago
 * @param 
 * @return cta_id, cta_producto, cta_estado, cta_titular, cta_fechaalta
 * @author  Fernando Aguirre
 * @version 1 Fernando Aguirre 19/7/21
 */
 
select cvn.cvn_cuenta
     , cvn.cvn_montofinal
     , cvn.cvn_fechaalta
  from Convenio cvn